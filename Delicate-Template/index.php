
<?php
$current_file = basename(__FILE__);
include "php/languages/german.php" ;
echo $txt_header;
?>
    
    <!-- **Main** -->
    <div id="main">
    	<!-- **Slider Section** -->
    	<section id="slider">
        
            <div id="layerslider-container-fw">    
                <div id="layerslider" style="width:100%; height:400px; margin:0px auto; ">
                        
                  <div class="ls-layer"  style="slidedirection:top; slidedelay:7000; durationin:1500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:500; transition3d:all;">      
                  
                      <img alt="layer slider" src="./images/modules/modules-3.jpg" class="ls-bg">
                      <!--<img alt="layer slider" class="ls-s-1" width="550" height="470" src="./images/modules/over-1.jpg"  style="position:absolute; top:79px; left:468px; slidedirection:top;   durationin:1500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0; ">-->
                        <h2 class="ls-s-1" style="position:absolute; top:235px; left:-4px; slidedirection:left;  durationin:0; durationout:0; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;"> Embeded Systems </h2>
                        <div class="ls-s-1 button-text2"  style="position:absolute; top:283.00001525878906px; left:149px; display:block; slidedirection:bottom;  durationin:0; durationout:0; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0">
                            <a style=""> Software development </a>
                        </div>  
                    </div>        
                    
                    <div class="ls-layer"  style="slidedirection:right; slidedelay:6000; durationin:1500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; timeshift:0; transition3d:all;">
                    
                        <img alt="layer slider" src="./images/modules/module-4.jpg" class="ls-bg">
                        <p class="ls-s-1" style="position:absolute; top:141px; left:548px; slidedirection:fade;  durationin:1500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;  width:420px; height:200px; background:#ffffff;  white-space:nowrap;">&nbsp; </p>
                        <h3 class="ls-s-1" style="position:absolute; top:158px; left:570px; slidedirection:top;  durationin:2000; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;"> Open source </h3>
                        <p class="ls-s-1 text1" style="position:absolute; top:200.00001525878906px; left:571px; slidedirection:right;  durationin:2500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0; width:380px; "> Embedded Linux </p>
                    </div>
                </div>
            </div>
        </section><!-- **Slider Section - End** -->
        
        <!-- **Container** -->
        <div class="container">
        
        <!-- **Primary Section** -->
        <section id="primary" class="content-full-width">     
        	<div class="intro-text type1">
                <h4> Herzlich Willkommen beim Ingenieurbüro Gregor Hadyk </h4>
                <h6>Das Ingenieurbüro Embedded Engineering wurde im Jahre 2012 gegründet.</br>
                Seither beschäftigen wir uns mit der Entwicklung von Embedded-Systemen.</br>
                Wir sind routiniert bei der Entwicklung von low-level Treibern oder Kommunikationsprotokollen für bare-metal,
                RTOS- oder Embedded-Linux-Systeme sowie der Erstellung von maßgeschneideten Applikationen zum Einsatz im professionellen Umfeld,
                vorwiegend auf Basis des QT-Frameworks zur Erstellung von GUIs bzw. zur Implementierung internetfähiger Netzwerkkommunikation.</br>
                Hierbei begleiten wir Ihr Projekt über den gesamten Lebenszyklus von der Konzeption</br>
                bis zur Auslieferung des fertigen Produktes.</br>

                Dank tiefgehende Kenntnisse in Software und Hardware Entwicklung, sowie Einsatz der Opensource Technologien.</br>
                Sind wir in Stande Ihr Projekt technisch wie auch ökonomisch maximal zu optimieren.</br>

                Dank absoluten Transparenz und Webbasierten Projektmanagement-Tools, können Sie jede Zeit den Verlauf und Fortschritt der Arbeiten an Ihrem Projekt
                verfolgen und mitgestallten.
                </h6>

            </div>
            
            <div class="hr-invisible-small"> </div>
            
            <div class="column one-fourth">
                <div class="ico-content type1">
                	<div class="icon"> <span class="icon-cogs"> </span> </div>
                    <h5> <a href="" title=""> Optimierung durch Open Source </a> </h5>
                    <p> Durch den Einsatz von Open Source Technologien arbeiten wir sehr effizient und ökonomisch an der Umsetzung Ihrer Ideen,
                        gleichzeitig sind wir aber auch sensibilisiert für die einhergehende lizenzrechtliche Verantwortung
                        und dokumentieren unsere Arbeit entsprechend </p>
                </div>
            </div>
            <div class="column one-fourth">
                <div class="ico-content type1">
                	<div class="icon"> <span class="icon-leaf"> </span> </div>
                    <h5> <a href="" title=""> Clean &amp; Simple </a> </h5>
                    <p> Bei unserer Arbeit orientieren wir uns an bestehenden Industriestandards und "best-practices",
                        für die verschiedenen Mikrokontroller verwenden wir Standard-Toolchains und entwickeln auf Basis der Programmiersprachen C und C++. </p>
                </div>
            </div>
            <div class="column one-fourth">
                <div class="ico-content type1">
                	<div class="icon"> <span class="icon-trophy"> </span> </div>
                    <h5> <a href="" title=""> Kundenorientierter Support </a> </h5>
                    <p> Projektverfolgung in der Cloud so wie Bugtracking und Content management tools ( Text anpassen ) </p>
                </div>
            </div>
            <div class="column one-fourth last">
                <div class="ico-content type1">
                	<div class="icon"> <span class="icon-laptop"> </span> </div>
                    <h5> <a href="" title=""> Transparent </a> </h5>
                    <p> Transparenz ist uns wichtig: Dank eines webbasierten Projektmanagement-Tools können Sie sich jederzeit einen
                        Überblick über den Fortschritt Ihres Projektes verschaffen und mit uns in Kontakt treten.
                    </p>
                </div>
            </div>
            
            <div class="clear"> </div>
            <div class="hr-invisible-small"> </div>

                <div class="carousel-arrows">
                    <a href="#" title="" class="portfolio-prev-arrow"> <span class="icon-chevron-left"> </span> </a>
                    <a href="#" title="" class="portfolio-next-arrow"> <span class="icon-chevron-right"> </span> </a>
                </div>
                    
            </div><!-- **Portfolio Carousel Wrapper - End** -->
            
            <div class="clear"> </div>
            <div class="hr-invisible"> </div>
            
            <div class="border-title"> <h2> From the blog <span> </span> </h2> </div>
            
            <div class="column one-half">
                <!-- **Blog Entry** -->
                <article class="blog-entry">                
                    <div class="entry-thumb-meta">
                        <div class="entry-thumb">
                            <a href="blog-single.html" title=""> <img src="http://placehold.it/340x230.jpg" alt="" title="" /> </a>
                        </div>
                        <div class="entry-meta"> 
                            <div class="date">
                                <span class="icon-calendar"> </span>
                                <p> April 06 2013 </p>
                            </div>
                            <a href="blog-single.html" title="" class="comments"> <span class="icon-comments"> </span> 16 </a>
                            <span class="rounded-bend"> </span>
                        </div>
                    </div>
                    
                    <div class="entry-details">                        
                        <div class="entry-title">
                            <h4> <a href="blog-single.html" title=""> Nullam dignisim vivera sceler </a> </h4>
                        </div>
                        
                        <div class="entry-metadata">
                            <div class="tags"> <span class="icon-tags"> </span> <a href="" title=""> teger</a>, <a href="" title=""> luctus</a>, <a href="" title=""> mor</a>, <a href="" title=""> purus </a> </div>
                            <!--<div class="categories"> <span class="icon-pushpin"> </span> <a href="" title=""> Business</a>, <a href="" title=""> Outdoors </a> </div>-->
                        </div>
                        
                        <div class="entry-body">
                            <p> Lorem ipsum dolor sit amet, consectet adipiscing elit. Praesent pulvinar massa nec augue malesuada in  </p>
                            <a href="blog-single.html" title="" class="read-more"> Read More <span class="icon-double-angle-right"> </span> </a>
                        </div>
                    </div>                    
                </article><!-- **Blog Entry - End** -->
            </div>
            
            <div class="column one-half last">
                <!-- **Blog Entry** -->
                <article class="blog-entry">                
                    <div class="entry-thumb-meta">
                        <div class="entry-thumb">
                            <a href="blog-single.html" title=""> <img src="http://placehold.it/340x230.jpg" alt="" title="" /> </a>
                        </div>
                        <div class="entry-meta"> 
                            <div class="date">
                                <span class="icon-calendar"> </span>
                                <p> April 06 2013 </p>
                            </div>
                            <a href="blog-single.html" title="" class="comments"> <span class="icon-comments"> </span> 16 </a>
                            <span class="rounded-bend"> </span>
                        </div>
                    </div>
                    
                    <div class="entry-details">                        
                        <div class="entry-title">
                            <h4> <a href="blog-single.html" title=""> Nullam dignisim vivera sceler </a> </h4>
                        </div>
                        
                        <div class="entry-metadata">
                            <div class="tags"> <span class="icon-tags"> </span> <a href="" title=""> teger</a>, <a href="" title=""> luctus</a>, <a href="" title=""> mor</a>, <a href="" title=""> purus </a> </div>
                        </div>
                        
                        <div class="entry-body">
                            <p> Lorem ipsum dolor sit amet, consectet adipiscing elit. Praesent pulvinar massa nec augue malesuada in  </p>
                            <a href="blog-single.html" title="" class="read-more"> Read More <span class="icon-double-angle-right"> </span> </a>
                        </div>
                    </div>                    
                </article><!-- **Blog Entry - End** -->
            </div>
            
            <div class="clear"> </div>
            <div class="hr-invisible"> </div>
            
            <div class="column one-half">
                <div class="border-title"> <h2> FAQ's <span> </span> </h2> </div>
                
                <!-- **Toggle Frame Set** -->  
                <div class="toggle-frame-set faq">
                    <h5 class="toggle-accordion"> <a href="#" title=""> How to center align a button? </a> </h5>
                    <div class="toggle-content">
                        <p> Consectetur adipiscing elit. Praesent pulvinar massa nec augue malsuada in aliquam odio interdum. </p>
                    </div>
                    <h5 class="toggle-accordion"> <a href="#" title=""> How to enable different types of homepage from backend? </a> </h5>
                    <div class="toggle-content">
                        <p> Consectetur adipiscing elit. Praesent pulvinar massa nec augue malsuada in aliquam odio interdum. </p>
                    </div>
                    <h5 class="toggle-accordion"> <a href="#" title=""> How to insert an image in the blog? </a> </h5>
                    <div class="toggle-content">
                        <p> Consectetur adipiscing elit. Praesent pulvinar massa nec augue malsuada in aliquam odio interdum. </p>
                    </div>
                    <h5 class="toggle-accordion"> <a href="#" title=""> How to set up slider? </a> </h5>
                    <div class="toggle-content">
                        <p> Consectetur adipiscing elit. Praesent pulvinar massa nec augue malsuada in aliquam odio interdum. </p>
                    </div>
                    <h5 class="toggle-accordion"> <a href="#" title=""> How to disable comments on pages? </a> </h5>
                    <div class="toggle-content">
                        <p> Consectetur adipiscing elit. Praesent pulvinar massa nec augue malsuada in aliquam odio interdum. </p>
                    </div>
                </div> <!-- **Toggle Frame Set - End** --> 
                
            </div>
            
            <div class="column one-half last">
                <div class="border-title"> <h2> Testimonials <span> </span> </h2> </div>
                
                <div class="testimonial">
                    <div class="author">
                        <img src="http://placehold.it/118x118.jpg" alt="" title="">
                    </div>                       
                    <blockquote>
                    	<q> Nunc at pretium est curabitur commodo leac est venenatis egestas sed aliquet auguevelit </q>
                        <cite> - Nullam dignissim </cite>
                    </blockquote>
                </div>
                <div class="testimonial">
                    <div class="author">
                        <img src="http://placehold.it/118x118.jpg" alt="" title="">
                    </div>                       
                    <blockquote>
                    	<q> Nunc at pretium est curabitur commodo leac est venenatis egestas sed aliquet auguevelit </q>
                        <cite> - Nullam dignissim </cite>
                    </blockquote>
                </div>
            </div>
            
            <div class="clear"> </div>
            <div class="hr-invisible"> </div>
            
            <div class="border-title"> <h2> Unsere Partner <span> </span> </h2> </div>
            
            <!-- **Partner Carousel Wrapper** -->
            <div class="partner-carousel-wrapper">

                <?php echo $partners ?>

                <div class="carousel-arrows">
                	<a href="" title="" class="partner-prev-arrow"> <span class="icon-chevron-left"> </span> </a>
                    <a href="" title="" class="partner-next-arrow"> <span class="icon-chevron-right"> </span> </a>
                </div>
            </div><!-- **Partner Carousel Wrapper - End** -->
            
        	
        </section><!-- **Primary Section** -->      
        
        </div><!-- **Container - End** -->
    </div><!-- **Main - End** -->
    
<?php echo $txt_footer ?>
	
</div><!-- **Wrapper - End** -->


<!-- **jQuery** -->
<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery.mobilemenu.js"></script>

<!-- Pretty Photo -->
<script src="js/jquery.prettyPhoto.js"></script>

<script src="js/jquery.carouFredSel-6.2.0-packed.js"></script>

<script src="js/twitter/jquery.tweet.min.js"></script>

<script src="js/custom.js"></script>

<!-- Layer Slider -->
<script src="js/jquery-easing-1.3.js"></script>
<script src="js/jquery-transit-modified.js"></script>
<script src="js/layerslider.transitions.js"></script>
<script src="js/layerslider.kreaturamedia.jquery.js"></script>
<script>
	jQuery(document).ready(function(){
		jQuery('#layerslider').layerSlider({
			skinsPath : 'layerslider-skins/',
			skin : 'fullwidthdark',
			thumbnailNavigation : 'hover',
			hoverPrevNext : false,
			responsive : false,
			responsiveUnder : 1120,
			sublayerContainer : 1060,
			width : '100%',
			height : '500px'
		});
	});		
</script>		



</body>
</html>
