<?php
$current_file = basename(__FILE__);
include "php/languages/german.php" ;
echo $txt_header;
?>
    
    <!-- **Main** -->
    <div id="main">
    
    	<!-- **Breadcrumb** -->
    	<section class="breadcrumb-section">
        	<div class="container">
            	<div class="breadcrumb">
                    <a href="index.html"> Home </a> 
                    <span class="icon-chevron-right"> </span>
                    <h1> Blog Two Column </h1>
                </div>
                <div class="main-phone-no">
                	<p> 1 (800) 567 8765 <br> <a href="" title=""> name@somemail.com </a> </p>
                </div>
            </div>
        </section><!-- **Breadcrumb** -->
    
        <!-- **Container** -->
        <div class="container">
        
        <!-- **Primary Section** -->
        <section id="primary" class="content-full-width">     
        
            <div class="column one-half">
                <!-- **Blog Entry** -->
                <article class="blog-entry">                
                    <div class="entry-thumb-meta">
                        <div class="entry-thumb">
                            <a href="blog-single.html" title=""> <img src="http://placehold.it/340x230.jpg" alt="" title="" /> </a>
                        </div>
                        <div class="entry-meta"> 
                            <div class="date">
                                <span class="icon-calendar"> </span>
                                <p> April 06 2013 </p>
                            </div>
                            <a href="blog-single.html" title="" class="comments"> <span class="icon-comments"> </span> 16 </a>
                            <span class="rounded-bend"> </span>
                        </div>
                    </div>
                    
                    <div class="entry-details">                        
                        <div class="entry-title">
                            <h4> <a href="blog-single.html" title=""> Nullam dignisim vivera sceler </a> </h4>
                        </div>
                        
                        <div class="entry-metadata">
                            <div class="tags"> <span class="icon-tags"> </span> <a href="" title=""> teger</a>, <a href="" title=""> luctus</a>, <a href="" title=""> mor</a>, <a href="" title=""> purus </a> </div>
                        </div>
                        
                        <div class="entry-body">
                            <p> Lorem ipsum dolor sit amet, consectet adipiscing elit. Praesent pulvinar massa nec augue malesuada in  </p>
                            <a href="blog-single.html" title="" class="read-more"> Read More <span class="icon-double-angle-right"> </span> </a>
                        </div>
                    </div>                    
                </article><!-- **Blog Entry - End** -->
                <div class="hr"> </div>
            </div>            
            
            <div class="column one-half last">
                <!-- **Blog Entry** -->
                <article class="blog-entry">                
                    <div class="entry-thumb-meta">
                        <div class="entry-thumb">
                            <a href="blog-single.html" title=""> <img src="http://placehold.it/340x230.jpg" alt="" title="" /> </a>
                        </div>
                        <div class="entry-meta"> 
                            <div class="date">
                                <span class="icon-calendar"> </span>
                                <p> April 06 2013 </p>
                            </div>
                            <a href="blog-single.html" title="" class="comments"> <span class="icon-comments"> </span> 16 </a>
                            <span class="rounded-bend"> </span>
                        </div>
                    </div>
                    
                    <div class="entry-details">                        
                        <div class="entry-title">
                            <h4> <a href="blog-single.html" title=""> Nullam dignisim vivera sceler </a> </h4>
                        </div>
                        
                        <div class="entry-metadata">
                            <div class="tags"> <span class="icon-tags"> </span> <a href="" title=""> teger</a>, <a href="" title=""> luctus</a>, <a href="" title=""> mor</a>, <a href="" title=""> purus </a> </div>
                        </div>
                        
                        <div class="entry-body">
                            <p> Lorem ipsum dolor sit amet, consectet adipiscing elit. Praesent pulvinar massa nec augue malesuada in  </p>
                            <a href="blog-single.html" title="" class="read-more"> Read More <span class="icon-double-angle-right"> </span> </a>
                        </div>
                    </div>                    
                </article><!-- **Blog Entry - End** -->
                <div class="hr"> </div>
            </div>
            
            <div class="column one-half">
                <!-- **Blog Entry** -->
                <article class="blog-entry">                
                    <div class="entry-thumb-meta">
                        <div class="entry-thumb">
                            <a href="blog-single.html" title=""> <img src="http://placehold.it/340x230.jpg" alt="" title="" /> </a>
                        </div>
                        <div class="entry-meta"> 
                            <div class="date">
                                <span class="icon-calendar"> </span>
                                <p> April 06 2013 </p>
                            </div>
                            <a href="blog-single.html" title="" class="comments"> <span class="icon-comments"> </span> 16 </a>
                            <span class="rounded-bend"> </span>
                        </div>
                    </div>
                    
                    <div class="entry-details">                        
                        <div class="entry-title">
                            <h4> <a href="blog-single.html" title=""> Nullam dignisim vivera sceler </a> </h4>
                        </div>
                        
                        <div class="entry-metadata">
                            <div class="tags"> <span class="icon-tags"> </span> <a href="" title=""> teger</a>, <a href="" title=""> luctus</a>, <a href="" title=""> mor</a>, <a href="" title=""> purus </a> </div>
                        </div>
                        
                        <div class="entry-body">
                            <p> Lorem ipsum dolor sit amet, consectet adipiscing elit. Praesent pulvinar massa nec augue malesuada in  </p>
                            <a href="blog-single.html" title="" class="read-more"> Read More <span class="icon-double-angle-right"> </span> </a>
                        </div>
                    </div>                    
                </article><!-- **Blog Entry - End** -->
                <div class="hr"> </div>
            </div>
            
            <div class="column one-half last">
                <!-- **Blog Entry** -->
                <article class="blog-entry">                
                    <div class="entry-thumb-meta">
                        <div class="entry-thumb">
                            <a href="blog-single.html" title=""> <img src="http://placehold.it/340x230.jpg" alt="" title="" /> </a>
                        </div>
                        <div class="entry-meta"> 
                            <div class="date">
                                <span class="icon-calendar"> </span>
                                <p> April 06 2013 </p>
                            </div>
                            <a href="blog-single.html" title="" class="comments"> <span class="icon-comments"> </span> 16 </a>
                            <span class="rounded-bend"> </span>
                        </div>
                    </div>
                    
                    <div class="entry-details">                        
                        <div class="entry-title">
                            <h4> <a href="blog-single.html" title=""> Nullam dignisim vivera sceler </a> </h4>
                        </div>
                        
                        <div class="entry-metadata">
                            <div class="tags"> <span class="icon-tags"> </span> <a href="" title=""> teger</a>, <a href="" title=""> luctus</a>, <a href="" title=""> mor</a>, <a href="" title=""> purus </a> </div>
                        </div>
                        
                        <div class="entry-body">
                            <p> Lorem ipsum dolor sit amet, consectet adipiscing elit. Praesent pulvinar massa nec augue malesuada in  </p>
                            <a href="blog-single.html" title="" class="read-more"> Read More <span class="icon-double-angle-right"> </span> </a>
                        </div>
                    </div>                    
                </article><!-- **Blog Entry - End** -->
                <div class="hr"> </div>
            </div>
            
            <div class="column one-half">
                <!-- **Blog Entry** -->
                <article class="blog-entry">                
                    <div class="entry-thumb-meta">
                        <div class="entry-thumb">
                            <a href="blog-single.html" title=""> <img src="http://placehold.it/340x230.jpg" alt="" title="" /> </a>
                        </div>
                        <div class="entry-meta"> 
                            <div class="date">
                                <span class="icon-calendar"> </span>
                                <p> April 06 2013 </p>
                            </div>
                            <a href="blog-single.html" title="" class="comments"> <span class="icon-comments"> </span> 16 </a>
                            <span class="rounded-bend"> </span>
                        </div>
                    </div>
                    
                    <div class="entry-details">                        
                        <div class="entry-title">
                            <h4> <a href="blog-single.html" title=""> Nullam dignisim vivera sceler </a> </h4>
                        </div>
                        
                        <div class="entry-metadata">
                            <div class="tags"> <span class="icon-tags"> </span> <a href="" title=""> teger</a>, <a href="" title=""> luctus</a>, <a href="" title=""> mor</a>, <a href="" title=""> purus </a> </div>
                        </div>
                        
                        <div class="entry-body">
                            <p> Lorem ipsum dolor sit amet, consectet adipiscing elit. Praesent pulvinar massa nec augue malesuada in  </p>
                            <a href="blog-single.html" title="" class="read-more"> Read More <span class="icon-double-angle-right"> </span> </a>
                        </div>
                    </div>                    
                </article><!-- **Blog Entry - End** -->
                <div class="hr"> </div>
            </div>
            
            <div class="column one-half last">
                <!-- **Blog Entry** -->
                <article class="blog-entry">                
                    <div class="entry-thumb-meta">
                        <div class="entry-thumb">
                            <a href="blog-single.html" title=""> <img src="http://placehold.it/340x230.jpg" alt="" title="" /> </a>
                        </div>
                        <div class="entry-meta"> 
                            <div class="date">
                                <span class="icon-calendar"> </span>
                                <p> April 06 2013 </p>
                            </div>
                            <a href="blog-single.html" title="" class="comments"> <span class="icon-comments"> </span> 16 </a>
                            <span class="rounded-bend"> </span>
                        </div>
                    </div>
                    
                    <div class="entry-details">                        
                        <div class="entry-title">
                            <h4> <a href="blog-single.html" title=""> Nullam dignisim vivera sceler </a> </h4>
                        </div>
                        
                        <div class="entry-metadata">
                            <div class="tags"> <span class="icon-tags"> </span> <a href="" title=""> teger</a>, <a href="" title=""> luctus</a>, <a href="" title=""> mor</a>, <a href="" title=""> purus </a> </div>
                        </div>
                        
                        <div class="entry-body">
                            <p> Lorem ipsum dolor sit amet, consectet adipiscing elit. Praesent pulvinar massa nec augue malesuada in  </p>
                            <a href="blog-single.html" title="" class="read-more"> Read More <span class="icon-double-angle-right"> </span> </a>
                        </div>
                    </div>                    
                </article><!-- **Blog Entry - End** -->
                <div class="hr"> </div>
            </div>
            
            <div class="column one-half">
                <!-- **Blog Entry** -->
                <article class="blog-entry">                
                    <div class="entry-thumb-meta">
                        <div class="entry-thumb">
                            <a href="blog-single.html" title=""> <img src="http://placehold.it/340x230.jpg" alt="" title="" /> </a>
                        </div>
                        <div class="entry-meta"> 
                            <div class="date">
                                <span class="icon-calendar"> </span>
                                <p> April 06 2013 </p>
                            </div>
                            <a href="blog-single.html" title="" class="comments"> <span class="icon-comments"> </span> 16 </a>
                            <span class="rounded-bend"> </span>
                        </div>
                    </div>
                    
                    <div class="entry-details">                        
                        <div class="entry-title">
                            <h4> <a href="blog-single.html" title=""> Nullam dignisim vivera sceler </a> </h4>
                        </div>
                        
                        <div class="entry-metadata">
                            <div class="tags"> <span class="icon-tags"> </span> <a href="" title=""> teger</a>, <a href="" title=""> luctus</a>, <a href="" title=""> mor</a>, <a href="" title=""> purus </a> </div>
                        </div>
                        
                        <div class="entry-body">
                            <p> Lorem ipsum dolor sit amet, consectet adipiscing elit. Praesent pulvinar massa nec augue malesuada in  </p>
                            <a href="blog-single.html" title="" class="read-more"> Read More <span class="icon-double-angle-right"> </span> </a>
                        </div>
                    </div>                    
                </article><!-- **Blog Entry - End** -->
                <div class="hr"> </div>
            </div>
            
            <div class="column one-half last">
                <!-- **Blog Entry** -->
                <article class="blog-entry">                
                    <div class="entry-thumb-meta">
                        <div class="entry-thumb">
                            <a href="blog-single.html" title=""> <img src="http://placehold.it/340x230.jpg" alt="" title="" /> </a>
                        </div>
                        <div class="entry-meta"> 
                            <div class="date">
                                <span class="icon-calendar"> </span>
                                <p> April 06 2013 </p>
                            </div>
                            <a href="blog-single.html" title="" class="comments"> <span class="icon-comments"> </span> 16 </a>
                            <span class="rounded-bend"> </span>
                        </div>
                    </div>
                    
                    <div class="entry-details">                        
                        <div class="entry-title">
                            <h4> <a href="blog-single.html" title=""> Nullam dignisim vivera sceler </a> </h4>
                        </div>
                        
                        <div class="entry-metadata">
                            <div class="tags"> <span class="icon-tags"> </span> <a href="" title=""> teger</a>, <a href="" title=""> luctus</a>, <a href="" title=""> mor</a>, <a href="" title=""> purus </a> </div>
                        </div>
                        
                        <div class="entry-body">
                            <p> Lorem ipsum dolor sit amet, consectet adipiscing elit. Praesent pulvinar massa nec augue malesuada in  </p>
                            <a href="blog-single.html" title="" class="read-more"> Read More <span class="icon-double-angle-right"> </span> </a>
                        </div>
                    </div>                    
                </article><!-- **Blog Entry - End** -->
                <div class="hr"> </div>
            </div>
            
            <div class="pagination"> 
            	<ul>
                    <li> <a href="" title=""> <span class="icon-double-angle-left"></span>Prev </a> </li>
                	<li> <a href="" title=""> 1 </a> </li>
                    <li class="active-page"> 2 </li>
                    <li> <a href="" title=""> 3 </a> </li>
                    <li> <a href="" title=""> 4 </a> </li>
                    <li> <a href="" title=""> Next<span class="icon-double-angle-right"></span>  </a> </li>
                </ul>
            </div>
        
        </section><!-- **Primary Section** -->      
        
        </div><!-- **Container - End** -->
    </div><!-- **Main - End** -->
    
    <!-- **Footer** -->
    <footer id="footer">
    	<div class="container">
        
			<div class="column one-fourth"> 
                <aside class="widget">
                    <h3 class="widgettitle"> <a href="" title=""> Information </a> </h3>
                    <ul>
                        <li> <a href="" title=""> Lorem ipsum dolor sit amet </a> </li>
                        <li> <a href="" title=""> Mauris aliquam nisi eget quam mollis </a> </li>
                        <li> <a href="" title=""> Sed a ante nulla, id pretium </a> </li>
                        <li> <a href="" title=""> Nunc eleifend purus cursus risus </a> </li>
                        <li> <a href="" title=""> Vestibulum in eros lectus, at tempus </a> </li>        
                        <li> <a href="" title=""> Nunc eu tellus vitae lacus viverra </a> </li>
                    </ul>
                </aside>   
			</div>
            
            <div class="column one-fourth"> 
                <aside class="widget">
                    <h3 class="widgettitle"> <a href="" title=""> Featured Products </a> </h3>
                    <ul class="product_list_widget">
                    	<li> 
                        	<a href="" title=""> 
                            	<img src="http://placehold.it/70x60.jpg" alt="" title="">                                 
                            </a>
                            Integer luctus portto pur nec ornare metus
                            <span class="amount"> $170,000 </span>
                        </li>
                        <li> 
                        	<a href="" title=""> 
                            	<img src="http://placehold.it/70x60.jpg" alt="" title="">                                 
                            </a>
                            Integer luctus portto pur nec ornare metus
                            <span class="amount"> $170,000 </span>
                        </li>
                        <li> 
                        	<a href="" title=""> 
                            	<img src="http://placehold.it/70x60.jpg" alt="" title="">                                 
                            </a>
                            Integer luctus portto pur nec ornare metus
                            <span class="amount"> $170,000 </span>
                        </li>
                    </ul>
                </aside>   
			</div>
        
        
        	<div class="column one-fourth">
                <aside class="widget tweetbox"> 
                    <h3 class="widgettitle"> <a href="" title=""> Twitter Feeds </a> </h3>
                    <div class="tweets"> </div>
                </aside>   
            </div>  
            
            <div class="column one-fourth last">
            	<aside class="widget">
                    <h3 class="widgettitle"> <a href="" title=""> Contact Info </a> </h3>
                	<p> 105, B Block,  Wesr Street, London, BCA, United Kingdom </p>
                    <p> <span class="icon-phone"> </span> Phone : 1800-299-234-22 </p>
                    <p> <span class="icon-envelope-alt"> </span> Email : <a href="mailto:delicate@someemail.com"> delicate@someemail.com </a> </p>
                    <p> <span class="icon-globe"> </span> Website : <a href="" title=""> www.yourdomain.com </a> </p> 
                    <ul class="social-icons">
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/facebook.png" alt="" title="">
                                <img src="images/sociable/facebook.png" alt="" title="">                        
                            </a>
                        </li>
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/google.png" alt="" title="">
                                <img src="images/sociable/google.png" alt="" title="">                        
                            </a>
                        </li>
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/twitter.png" alt="" title="">
                                <img src="images/sociable/twitter.png" alt="" title="">                        
                            </a>
                        </li>
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/dribble.png" alt="" title="">
                                <img src="images/sociable/dribble.png" alt="" title="">                        
                            </a>
                        </li>
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/linkedin.png" alt="" title="">
                                <img src="images/sociable/linkedin.png" alt="" title="">                        
                            </a>
                        </li>
                    </ul>
                </aside>	
            </div>
            
        </div>
        
        <div class="copyright">        	
        	<div class="container">
                <p> Copyright &copy; 2013 Delicate Theme All Rights Reserved. | <a href="" title=""> Design Themes </a> </p>        	
            </div>
        </div>
    </footer><!-- **Footer - End** -->
	
</div><!-- **Wrapper - End** -->


<!-- **jQuery** -->
<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery.mobilemenu.js"></script>

<script src="js/jquery.viewport.js"></script>

<script src="js/twitter/jquery.tweet.min.js"></script>

<script src="js/custom.js"></script>

</body>
</html>
