<?php

$current_elements = array();

$dirs = scandir('./');

foreach ($dirs as $value)
{
    if($value == $current_file)
    {
        $current_elements[$value] = ' class="current_page_item" ';
    }
    else
    {
        $current_elements[$value] = '';
    }
}

$txt_header ='<!doctype html>
              <!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
              <!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
              <!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
              <!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

              <head>
              	<meta charset="utf-8">
              	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

              	<title> Embedded Engineering </title>

              	<meta name="description" content="Ingenieurbüro Gregor Hadyk">
              	<meta name="author" content="Gregor Hadyk">

                  <!--[if lt IE 9]>
                      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
                  <![endif]-->

                  <!-- **Favicon** -->
                  <link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />

                  <!-- **CSS - stylesheets** -->
                  <link id="default-css" href="style.css" rel="stylesheet" media="all" />
                  <link id="shortcodes-css" href="shortcodes.css" rel="stylesheet" media="all" />
                  <link id="skin-css" href="skins/green/style.css" rel="stylesheet" media="all" />

                  <!-- **Additional - stylesheets** -->
                  <link rel="stylesheet" href="css/prettyPhoto.css" media="screen" />
                  <link rel="stylesheet" href="css/layerslider.css" type="text/css">
                  <!--[if lt IE 9]>
                      <script src="js/html5.js"></script>
                  <![endif]-->
                  <link href="responsive.css" rel="stylesheet" media="all" />

                  <!-- **Font Awesome** -->
                  <link rel="stylesheet" href="css/font-awesome.min.css">
                  <!--[if IE 7]>
                  <link rel="stylesheet" href="css/font-awesome-ie7.min.css">
                  <![endif]-->

                  <!-- **Google - Fonts** -->
                  <link href="http://fonts.googleapis.com/css?family=Arvo:400,700" rel="stylesheet" type="text/css">

              </head>

              <body>

              <!-- **Wrapper** -->
              <div class="wrapper">

              	<!-- **Header** -->
              	<header id="header">

                  	<!-- **Top Bar** -->
                  	<div style="height: 35px;" id="top-bar">
                      	<div class="container">
                          	<p class="phone-no"> +49 (0) 173 2175650 <a href="mailto:info@embedded-engineering.com"> info@embedded-engineering.com </a> </p>
                          </div>
                      </div><!-- **Top Bar - End** -->

                      <div class="container">
                      	<!-- **Logo - End** -->
                          <div id="logo">
                          	<h2> <a href="index.php" title=""> Embedded <span> Engineering </span> </a> </h2>
                              <h4> <a href="index.php" title=""> Ingenieurbüro <span> Gregor Hadyk </span> </a> </h4>
                          </div><!-- **Logo - End** -->

                          <!-- **Navigation** -->
                          <nav id="main-menu">
                              <ul>
                                  <li '.$current_elements["index.php"].'> <a href="index.php" title=""> Home </a> <span> </span>
                                  </li>
                                  <li '.$current_elements["about.php"].'> <a href="about.php" title=""> Unternehmen </a> <span> </span>
                                      <ul>
                                          <li> <a href="team.php" title=""> Team</a> </li>
                                      </ul>
                                  </li>
                                  <!--
                                  <li '.$current_elements["shortcodes-typography.php"].'> <a href="shortcodes-typography.php" title=""> Projekte </a> <span> </span>
                                  </li>
                                  <li '.$current_elements["portfolio-four-column.php"].'> <a href="portfolio-four-column.php" title=""> Dienstleistung </a> <span> </span>
                                      <ul>
                                          <li> <a href="portfolio-single.php" title=""> Softwareentwicklung </a> </li>
                                          <li> <a href="portfolio-single2.php" title=""> Beratung </a> </li>
                                      </ul>
                                  </li>
                                  <li '.$current_elements["blog-thumb.php"].'> <a href="blog-thumb.php" title=""> Blog </a> <span> </span>
                                  </li>
                                  -->
                                  <li '.$current_elements["contact.php"].'> <a href="contact.php" title=""> Kontakt </a> <span> </span>
                                  </li>
                                  <li '.$current_elements["impressum.php"].'> <a href="impressum.php" title=""> Impressum </a> <span> </span>
                                  </li>
                              </ul>
                          </nav><!-- **Navigation - End** -->
                      </div>

                  </header><!-- **Header - End** -->';

$txt_footer = '    <!-- **Footer** -->
    <footer id="footer">
      <div class="container">
            <div class="column one-two" >
                <aside class="widget">
                    <h3 class="widgettitle"> <a href="" title=""> Leistungsspektrum </a> </h3>
                    <a href="" title=""> Softwareentwicklung  </a></br>
                    <a href="" title=""> Projektoptimierung  </a></br>
                    <a href="" title=""> Beratung  </a></br>
                    <a href="" title=""> Hardwareplannung  </a></br>
                    <a href="" title=""> Schulungen  </a></br>
                </aside>
            </div>

            <div class="column one-two last" style="float: right">
              <aside class="widget">
                    <h3 class="widgettitle"> <a href="" title=""> Kontaktinformationen </a> </h3>
                  <p> Vogelsbergstrasse 83</p>
                    <p> 50765 Köln </p>
                    <p> <span class="icon-phone"> </span> Telefon : +49 (0) 173 2175650 </p>
                    <p> <span class="icon-envelope-alt"> </span> Email : <a href="mailto:info@embedded-engineering.com"> info@embedded-engineering.com </a> </p>
                    <p> <span class="icon-globe"> </span> Webseite : <a href="http://www.embedded-engineering.com" title=""> www.embedded-engineering.com </a> </p>
                    <a style="margin-right: 20px;" href="https://www.xing.com/profile/Gregor_Hadyk" title=""><img style="margin-bottom: 6px;" src="https://www.xing.com/img/buttons/9_de_btn.gif"  alt="" title=""></a>
                    <a href="https://www.linkedin.com/in/gregor-hadyk-1b6a1550" title=""><img src="images/sociable/linked-logo.png" width="120" alt="" title=""></a>
                </aside>  
            </div>
        </div>
        
        <div class="copyright">         
          <div class="container">
                <p> &copy; Embedded Engineering <a href="http://www.embedded-engineering.com" title=""> Ingenierubüro Gregor Hadyk </a> </p>
            </div>
        </div>
    </footer><!-- **Footer - End** -->';

$breadcrumb = '<!-- **Breadcrumb** -->
    	<section class="breadcrumb-section">
        	<div class="container">
            	<div class="breadcrumb">
                    <a href="index.php"> Home </a>
                    <span class="icon-chevron-right"> </span>
                    <h1> Kontakt </h1>
                </div>
                <div class="main-phone-no">
                	<p> +49(0)173 2175650 <br> <a href="mailto:info@ibgh.biz" title=""> info@ibgh.biz </a> </p>
                </div>
            </div>
        </section><!-- **Breadcrumb** -->';

$partners = '<ul class="partner-carousel">
                	<li> <a href="http://www.rtw.com" title="RTW GmbH & Co. KG"> <img width="200" height="40" src="./images/partners/rtw.png" alt="" title=""> </a> </li>
                    <li> <a href="http://www.systecnet.com" title="SysTec Systemtechnik und Industrieautomation GmbH"> <img  width="200" height="50" src="./images/partners/systec.png" alt="" title=""> </a> </li>
                    <li> <a href="http://www.it-projekt-eg.de" title="IT-Projektgenossenschaft"> <img width="200" height="40" src="./images/partners/itpg-logo.png" alt="" title=""> </a> </li>
                    <li> <a href="https://www.bontronic.de" title="Bontronic Steuerungstechnik GmbH"> <img width="180" height="50" src="./images/partners/logo-bontronic.png" alt="" title=""> </a> </li>
                    <li> <a href="http://www.tls-electronics.de" title="TLS electronics GmbH"> <img width="180" height="55" src="./images/partners/tls-logo.png" alt="" title=""> </a> </li>
                </ul>';
