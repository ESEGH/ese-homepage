<?php
$current_file = basename(__FILE__);
include "php/languages/german.php" ;
echo $txt_header;
?>

    <!-- **Main** -->
    <div id="main">

        <?php echo $breadcrumb; ?>
    
        <!-- **Container** -->
        <div class="container">
        
        <!-- **Primary Section** -->
        <section id="primary" class="content-full-width">     
        
            
            <div class="border-title"> <h2> Headings <span> </span> </h2> </div>   
            
            <div class="column one-half">
            	<div class="border-title"> <h1> Heading1 <span> </span> </h1> </div>
            	<div class="border-title"> <h2> Heading2 <span> </span> </h2> </div>
            	<div class="border-title"> <h3> Heading3 <span> </span> </h3> </div>
            	<div class="border-title"> <h4> Heading4 <span> </span> </h4> </div>
            </div>
            
            <div class="column one-half last">
            	<h1> H1 : Lorem ipsum dolor sit amet </h1>
                <div class="hr-invisible-very-small"> </div>
                <h2> H2 : Lorem ipsum dolor sit amet </h2>
                <div class="hr-invisible-very-small"> </div>
                <h3> H3 : Lorem ipsum dolor sit amet </h3>
                <div class="hr-invisible-very-small"> </div>
                <h4> H4 : Lorem ipsum dolor sit amet </h4>
                <div class="hr-invisible-very-small"> </div>
                <h5> H5 : Lorem ipsum dolor sit amet </h5>
                <div class="hr-invisible-very-small"> </div>
                <h6> H6 : Lorem ipsum dolor sit amet </h6>
            </div>
            
            <div class="clear"> </div>
            <div class="hr-invisible"> </div>
            
            
            <div class="border-title"> <h2> Tables <span> </span> </h2> </div>   
            
            <table>
                <thead>
                    <tr> 
                        <th> Header1 </th>
                        <th> Header2 </th>
                        <th> Header3 </th>
                        <th> Header4 </th>
                        <th> Header5 </th>
                    </tr>                            
                </thead>
                <tbody>
                    <tr> 
                        <td> Division1 </td>
                        <td> Division2 </td>
                        <td> Division3 </td>
                        <td> Division4 </td>
                        <td> Division5 </td>
                    </tr>  
                    <tr> 
                        <td> Division1 </td>
                        <td> Division2 </td>
                        <td> Division3 </td>
                        <td> Division4 </td>
                        <td> Division5 </td>
                    </tr>   
                    <tr> 
                        <td> Division1 </td>
                        <td> Division2 </td>
                        <td> Division3 </td>
                        <td> Division4 </td>
                        <td> Division5 </td>
                    </tr>   
                    <tr> 
                        <td> Division1 </td>
                        <td> Division2 </td>
                        <td> Division3 </td>
                        <td> Division4 </td>
                        <td> Division5 </td>
                    </tr>                             
                </tbody>
            </table>
            
            <div class="clear"> </div>
            <div class="hr-invisible-very-small"> </div>            
            
            <table>
                <thead>
                    <tr> 
                        <th> Header1 </th>
                        <th> Header2 </th>
                        <th> Header3 </th>
                        <th> Header4 </th>
                    </tr>                            
                </thead>
                <tbody>
                    <tr> 
                        <td> Division1 </td>
                        <td> Division2 </td>
                        <td> Division3 </td>
                        <td> Division4 </td>
                    </tr>  
                    <tr> 
                        <td> Division1 </td>
                        <td> Division2 </td>
                        <td> Division3 </td>
                        <td> Division4 </td>
                    </tr>   
                    <tr> 
                        <td> Division1 </td>
                        <td> Division2 </td>
                        <td> Division3 </td>
                        <td> Division4 </td>
                    </tr>   
                    <tr> 
                        <td> Division1 </td>
                        <td> Division2 </td>
                        <td> Division3 </td>
                        <td> Division4 </td>
                    </tr>                             
                </tbody>
            </table>
            
            <div class="clear"> </div>
            <div class="hr-invisible"> </div>
            <div class="border-title"> <h2> Columns <span> </span> </h2> </div>  
            
            <div  class="column one-half">
                <h3>Column 1/2</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur massa aliquam et. Sed sit amet est et turpis viverra consequat. Quisque diam eros, rhoncus sit amet rutrum ut, sagittis a erat. 
            </div>
            <div  class="column one-half  last">
                <h3>Column 1/2</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur massa aliquam et. Sed sit amet est et turpis viverra consequat. Quisque diam eros, rhoncus sit amet rutrum ut, sagittis a erat.</p>
            </div>
            <div class="clear"></div>
            <div class="hr-invisible"></div>
            <div  class="column one-third">
                <h3>Column 1/3</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur massa aliquam et. Sed sit amet est et turpis viverra consequat.</p>
            </div>
            <div  class="column one-third">
                <h3>Column 1/3</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur massa aliquam et. Sed sit amet est et turpis viverra consequat.</p>
            </div>
            <div  class="column one-third  last">
                <h3>Column 1/3</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur massa aliquam et. Sed sit amet est et turpis viverra consequat.</p>
            </div>
            <div class="clear"></div>
            <div class="hr-invisible"></div>
            <div  class="column one-third">
                <h3>Column 1/3</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur massa aliquam et. Sed sit amet est et turpis viverra consequat.</p>
            </div>
            <div  class="column two-third  last">
                <h3>Column 2/3</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur massa aliquam et. Sed sit amet est et turpis viverra consequat. Quisque diam eros, rhoncus sit amet rutrum ut, sagittis a erat. Sed lobortis, arcu id rutrum dictum, tellus mauris hendrerit urna, sit amet commodo orci velit et urna. Quisque elementum, odio porttitor varius gravida, ante diam facilisis erat, in fermentum mi elit in magna.</p>
            </div>
            <div class="clear"></div>
            <div class="hr-invisible"></div>
            <div  class="column one-fourth">
            <h3>Column 1/4</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur massa aliquam et.</p>
            </div>
            <div  class="column one-fourth">
                <h3>Column 1/4</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur massa aliquam et.</p>
            </div>
            <div  class="column one-fourth">
                <h3>Column 1/4</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur massa aliquam et.</p>
            </div>
            <div  class="column one-fourth  last">
                <h3>Column 1/4</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur massa aliquam et.</p>
            </div>
            <div class="clear"></div>
            <div class="hr-invisible"></div>
            <div  class="column one-fourth">
                <h3>Column 1/4</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur massa aliquam et.</p>
            </div>
            <div  class="column three-fourth  last">
                <h3>Column 3/4</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur massa aliquam et. Sed sit amet est et turpis viverra consequat. Quisque diam eros, rhoncus sit amet rutrum ut, sagittis a erat. Sed lobortis, arcu id rutrum dictum, tellus mauris hendrerit urna, sit amet commodo orci velit et urna. Quisque elementum, odio porttitor varius gravida, ante diam facilisis erat, in fermentum mi elit in magna.</p>
            </div>
            <div class="clear"></div>
            <div class="hr-invisible"></div>
            <div  class="column one-fourth">
                <h3>Column 1/4</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur massa aliquam et.</p>
            </div>
            <div  class="column one-fourth">
                <h3>Column 1/4</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur massa aliquam et.</p>
            </div>
            <div  class="column one-half  last">
                <h3>Column 1/2</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur massa aliquam et. Sed sit amet est et turpis viverra consequat. Quisque diam eros, rhoncus sit amet rutrum ut, sagittis a erat.</p>
            </div>
            <div class="clear"></div>
            <div class="hr-invisible"></div>
            <div  class="column one-fifth">
                <h3>Column 1/5</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column one-fifth">
                <h3>Column 1/5</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column one-fifth">
                <h3>Column 1/5</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column one-fifth">
                <h3>Column 1/5</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column one-fifth  last">
                <h3>Column 1/5</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div class="clear"></div>
            <div class="hr-invisible"></div>
            <div  class="column one-fifth">
                <h3>Column 1/5</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column one-fifth">
                <h3>Column 1/5</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column one-fifth">
                <h3>Column 1/5</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column two-fifth  last">
                <h3>Column 2/5</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non justo vitae odio fringilla dapibus id nec est. Integer eu felis lectus, quis sagittis nisl. Morbi elit mauris, sollicitudin ut facilisis eu, vehicula sit amet libero.</p>
            </div>
            <div class="clear"></div>
            <div class="hr-invisible"></div>
            <div  class="column one-fifth">
                <h3>Column 1/5</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column one-fifth">
                <h3>Column 1/5</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column three-fifth  last">
                <h3>Column 3/5</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non justo vitae odio fringilla dapibus id nec est. Integer eu felis lectus, quis sagittis nisl. Morbi elit mauris, sollicitudin ut facilisis eu, vehicula sit amet libero. Suspendisse auctor, mauris non condimentum iaculis, nulla urna varius orci, in convallis.</p>
            </div>
            <div class="clear"></div>
            <div class="hr-invisible"></div>
            <div  class="column one-fifth">
                <h3>Column 1/5</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column four-fifth  last">
                <h3>Column 4/5</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non justo vitae odio fringilla dapibus id nec est. Integer eu felis lectus, quis sagittis nisl. Morbi elit mauris, sollicitudin ut facilisis eu, vehicula sit amet libero. Suspendisse auctor, mauris non condimentum iaculis, nulla urna varius orci, in convallis ipsum magna ac arcu. Aenean mollis diam vel lacus placerat cursus. Pellentesque pulvinar suscipit dui, venenatis elementum sem placerat sit amet.</p>
            </div>
            <div class="clear"></div>
            <div class="hr-invisible"></div>
            <div  class="column one-fifth">
                <h3>Column 1/5</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column two-fifth">
                <h3>Column 2/5</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non justo vitae odio fringilla dapibus id nec est. Integer eu felis lectus, quis sagittis nisl. Morbi elit mauris, sollicitudin ut facilisis eu, vehicula sit amet libero.</p>
            </div>
            <div  class="column two-fifth  last">
                <h3>Column 2/5</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non justo vitae odio fringilla dapibus id nec est. Integer eu felis lectus, quis sagittis nisl. Morbi elit mauris, sollicitudin ut facilisis eu, vehicula sit amet libero.</p>
            </div>
            <div class="clear"></div>
            <div class="hr-invisible"></div>
            <div  class="column two-fifth">
                <h3>Column 2/5</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non justo vitae odio fringilla dapibus id nec est. Integer eu felis lectus, quis sagittis nisl. Morbi elit mauris, sollicitudin ut facilisis eu, vehicula sit amet libero.</p>
            </div>
            <div  class="column three-fifth  last">
                <h3>Column 3/5</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non justo vitae odio fringilla dapibus id nec est. Integer eu felis lectus, quis sagittis nisl. Morbi elit mauris, sollicitudin ut facilisis eu, vehicula sit amet libero. Suspendisse auctor, mauris non condimentum iaculis, nulla urna varius orci, in convallis.</p>
            </div>
            <div class="clear"></div>
            <div class="hr-invisible"></div>
            <div  class="column one-fifth">
                <h3>Column 1/5</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column one-fifth">
                <h3>Column 1/5</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column three-fifth  last">
                <h3>Column 3/5</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non justo vitae odio fringilla dapibus id nec est. Integer eu felis lectus, quis sagittis nisl. Morbi elit mauris, sollicitudin ut facilisis eu, vehicula sit amet libero. Suspendisse auctor, mauris non condimentum iaculis, nulla urna varius orci, in convallis.</p>
            </div>
            <div class="clear"></div>
            <div class="hr-invisible"></div>
            <div  class="column one-sixth">
                <h3>Column 1/6</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column one-sixth">
                <h3>Column 1/6</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column one-sixth">
                <h3>Column 1/6</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column one-sixth">
                <h3>Column 1/6</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column one-sixth">
                <h3>Column 1/6</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column one-sixth  last">
                <h3>Column 1/6</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div class="clear"></div>
            <div class="hr-invisible"></div>
            <div  class="column one-sixth">
                <h3>Column 1/6</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column one-sixth">
                <h3>Column 1/6</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column one-sixth">
                <h3>Column 1/6</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column one-sixth">
                <h3>Column 1/6</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column two-sixth  last">
                <h3>Column 2/6</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div class="clear"></div>
            <div class="hr-invisible"></div>
            <div  class="column one-sixth">
                <h3>Column 1/6</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column one-sixth">
                <h3>Column 1/6</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column one-sixth">
                <h3>Column 1/6</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column three-sixth  last">
                <h3>Column 3/6</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div class="clear"></div>
            <div class="hr-invisible"></div>
            <div  class="column one-sixth">
                <h3>Column 1/6</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column one-sixth">
                <h3>Column 1/6</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column four-sixth  last">
                <h3>Column 4/6</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div class="clear"></div>
            <div class="hr-invisible"></div>
            <div  class="column one-sixth">
                <h3>Column 1/6</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column five-sixth  last">
                <h3>Column 5/6</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div class="clear"></div>
            <div class="hr-invisible"></div>
            <div  class="column one-sixth">
                <h3>Column 1/6</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column two-sixth">
                <h3>Column 2/6</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column three-sixth  last">
                <h3>Column 3/6</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div class="clear"></div>
            <div class="hr-invisible"></div>
            <div  class="column three-sixth">
                <h3>Column 3/6</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column three-sixth  last">
                <h3>Column 3/6</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div class="clear"></div>
            <div class="hr-invisible"></div>
            <div  class="column two-sixth">
                <h3>Column 2/6</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
            <div  class="column four-sixth  last">
                <h3>Column 4/6</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sagittis elementum est, eu consectetur.</p>
            </div>
        
        </section><!-- **Primary Section** -->      
        
        </div><!-- **Container - End** -->
    </div><!-- **Main - End** -->

<?php echo $txt_footer ?>
	
</div><!-- **Wrapper - End** -->


<!-- **jQuery** -->
<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery.mobilemenu.js"></script>
<script src="js/jquery.viewport.js"></script>

<script src="js/jquery.cookie.js"></script>
<script src="js/controlpanel.js"></script>

<script src="js/jquery.tabs.min.js"></script>

<script src="js/twitter/jquery.tweet.min.js"></script>

<script src="js/custom.js"></script>

</body>
</html>
