<?php
$current_file = basename(__FILE__);
include "php/languages/german.php" ;
echo $txt_header;
?>
    
    <!-- **Main** -->
    <div id="main">

        <?php echo $breadcrumb; ?>
    
        <!-- **Container** -->
        <div class="container">
        
        <!-- **Primary Section** -->
        <section id="primary" class="content-full-width">     
        
        	<div class="page-slider-container">
                <ul class="page-slider">
                    <li> <img width="1060" height="300" src="./images/cologne.png" alt="" title=""> </li>
                    <li> <img width="1060" height="300" src="./images/80.jpeg" alt="" title=""> </li>
                </ul>                
            </div>
        
            <div class="clear"> </div>
            <div class="hr-invisible-small"> </div>
            
            <div class="column one-half">
                <div class="border-title"> <h2> Warum sollten Sie uns beauftragen <span> </span> </h2> </div>
                <!-- **Toggle Frame Set** -->
                <div class="toggle-frame-set">
                	<div class="toggle-frame">
                        <h5 class="toggle-accordion"> <a href="#" title=""> Ausgezeichnetes Support </a> </h5>
                        <div class="toggle-content">
                            <p> Wir unterstützen Sie ... ( Text anpassen ) </p>
                        </div>
                    </div>
                	<div class="toggle-frame">
                        <h5 class="toggle-accordion"> <a href="#" title=""> Kommunikation </a> </h5>
                        <div class="toggle-content">
                            <p> Regelmässige Updates zum Stand des Projektes. Online Status ... ( Text anpassen ) </p>
                        </div>
                    </div>
                	<div class="toggle-frame">
                        <h5 class="toggle-accordion"> <a href="#" title=""> Kundenorientierung </a> </h5>
                        <div class="toggle-content">
                            <p> Jedes Projekt ist anders. Wir erarbeiten für Sie eine Kundenspezifische Lösung ... ( Text anpassen ) </p>
                        </div>
                    </div>
                	<div class="toggle-frame">                    
                        <h5 class="toggle-accordion"> <a href="#" title=""> Qualität der Software  </a> </h5>
                        <div class="toggle-content">
                            <p> Automatisch etests erlauben uns einen hochen Mass an Qualität einzuhalten. </p>
                        </div>
                    </div>
                </div> <!-- **Toggle Frame Set - End** -->                 
            </div>
            
            <div class="column one-half last">
                <div class="border-title"> <h2> Unsere Kernkompetenzen <span> </span> </h2> </div>
                
                <div class="progress progress-striped">
                	<div data-value="99" class="bar">
                    	<div class="bar-text"> Embedded Systems <span> 99% </span> </div>
                    </div>
                </div>
                <div class="progress progress-striped">
                	<div data-value="95" class="bar">
                    	<div class="bar-text"> C/C++ <span> 95% </span> </div>
                    </div>
                </div>
                <div class="progress progress-striped">
                    <div data-value="80" class="bar">
                        <div class="bar-text"> IOT <span> 80% </span> </div>
                    </div>
                </div>
                <div class="progress progress-striped">
                	<div data-value="70" class="bar">
                    	<div class="bar-text"> Hardware design <span> 70% </span> </div>
                    </div>
                </div>
                <div class="progress progress-striped">
                	<div data-value="70" class="bar">
                    	<div class="bar-text"> Applikationsentwicklung <span> 70% </span> </div>
                    </div>
                </div>
            </div>
            
            <div class="clear"> </div>
            <div class="hr-invisible"> </div>
            
            <div class="border-title"> <h2> Unseres Leistungsspektrum <span> </span> </h2> </div>
            
            <div class="column one-third">
                <div class="ico-content type2">
                	<div class="icon"> <span class="icon-cogs"> </span> </div>
                    <h5> <a href="" title=""> Embedded Software Entwicklung </a> </h5>
                    <p> Lorem ipsum dolor sit amet, consectet adipiscing elit. Etiam quis placerat urna. Nulla nulla diam, adipiscing non ornardfe non, commodo </p>
                </div>
            </div>
            <div class="column one-third">
                <div class="ico-content type2">
                	<div class="icon"> <span class="icon-beaker"> </span> </div>
                    <h5> <a href="" title=""> Open Source </a> </h5>
                    <p> Lorem ipsum dolor sit amet, consectet adipiscing elit. Etiam quis placerat urna. Nulla nulla diam, adipiscing non ornardfe non, commodo </p>
                </div>
            </div>
            <div class="column one-third last">
                <div class="ico-content type2">
                	<div class="icon"> <span class="icon-magic"> </span> </div>
                    <h5> <a href="" title=""> Applikationsentwicklung </a> </h5>
                    <p> Lorem ipsum dolor sit amet, consectet adipiscing elit. Etiam quis placerat urna. Nulla nulla diam, adipiscing non ornardfe non, commodo </p>
                </div>
            </div>
            
            <div class="column one-third">
                <div class="ico-content type2">
                	<div class="icon"> <span class="icon-magnet"> </span> </div>
                    <h5> <a href="" title=""> Embedde Linux Treiber </a> </h5>
                    <p> Lorem ipsum dolor sit amet, consectet adipiscing elit. Etiam quis placerat urna. Nulla nulla diam, adipiscing non ornardfe non, commodo </p>
                </div>
            </div>
            <div class="column one-third">
                <div class="ico-content type2">
                	<div class="icon"> <span class="icon-dashboard"> </span> </div>
                    <h5> <a href="" title=""> RTOS </a> </h5>
                    <p> Lorem ipsum dolor sit amet, consectet adipiscing elit. Etiam quis placerat urna. Nulla nulla diam, adipiscing non ornardfe non, commodo </p>
                </div>
            </div>
            <div class="column one-third last">
                <div class="ico-content type2">
                	<div class="icon"> <span class="icon-trophy"> </span> </div>
                    <h5> <a href="" title=""> Hardwareplannung </a> </h5>
                    <p> Lorem ipsum dolor sit amet, consectet adipiscing elit. Etiam quis placerat urna. Nulla nulla diam, adipiscing non ornardfe non, commodo </p>
                </div>
            </div>
            
            <div class="clear"> </div>
            <div class="hr-invisible"> </div>
            
            <div class="border-title"> <h2> Unsere Partner <span> </span> </h2> </div>
            
            <!-- **Partner Carousel Wrapper** -->
            <div class="partner-carousel-wrapper">
                <?php echo $partners ?>
                <div class="carousel-arrows">
                	<a href="" title="" class="partner-prev-arrow"> <span class="icon-chevron-left"> </span> </a>
                    <a href="" title="" class="partner-next-arrow"> <span class="icon-chevron-right"> </span> </a>
                </div>
            </div><!-- **Partner Carousel Wrapper - End** -->
        
        </section><!-- **Primary Section** -->      
        
        </div><!-- **Container - End** -->
    </div><!-- **Main - End** -->

<?php echo $txt_footer ?>
	
</div><!-- **Wrapper - End** -->


<!-- **jQuery** -->
<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/jquery.js"></script>

<script src="js/jquery.mobilemenu.js"></script>
<script src="js/jquery.bxslider.min.js"></script>
<script src="js/jquery.carouFredSel-6.2.0-packed.js"></script>
<script src="js/jquery.viewport.js"></script>
<script src="js/twitter/jquery.tweet.min.js"></script>
<script src="js/custom.js"></script>

</body>
</html>
