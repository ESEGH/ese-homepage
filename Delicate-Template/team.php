<?php
$current_file = basename(__FILE__);
include "php/languages/german.php" ;
echo $txt_header;
?>
    
    <!-- **Main** -->
    <div id="main">

        <?php echo $breadcrumb; ?>
    
        <!-- **Container** -->
        <div class="container">
        
        <!-- **Primary Section** -->
        <section id="primary" class="content-full-width">     

            <div class="column one-third">
            	<!-- **Team** -->   
            	<div class="team">
                	<div class="image">
	                	<img src="./images/team/Foto%205.JPG" alt="" title="">
                    </div>
                    <h4> Gregor Hadyk </h4>
                    <h6> Dipl.Ing. / Softwareentwickler </h6>
                    <ul class="social-icons">
                        <!--
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/twitter.png" alt="" title="">
                                <img src="images/sociable/team-social/twitter.png" alt="" title="">                        
                            </a>
                        </li>
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/facebook.png" alt="" title="">
                                <img src="images/sociable/team-social/facebook.png" alt="" title="">                        
                            </a>
                        </li>
                        -->
                        <li>
                            <a href="https://www.xing.com/profile/Gregor_Hadyk" title="Xing Gregor Hadyk">
                                <img src="images/sociable/hover/xing.png" alt="" title="">
                                <img src="images/sociable/hover/xing.png" alt="" title="">
                            </a>
                        </li>
                        <!--
                        <li>
                            <a href="" title="">
                                <img src="images/sociable/hover/google.png" alt="" title="">
                                <img src="images/sociable/team-social/google.png" alt="" title="">
                            </a>
                        </li>
                        -->
                    </ul>
                </div><!-- **Team - End** -->   
            </div>
            
            <div class="column one-third">
            	<!-- **Team** -->   
            	<div class="team">
                	<div class="image">
	                	<img src="./images/team/Foto%2011.JPG" alt="" title="">
                    </div>
                    <h4> Aneta Hadyk </h4>
                    <h6> Buchhaltung </h6>
                    <ul class="social-icons">
                        <!--
                        <li>
                            <a href="" title="">
                                <img src="images/sociable/hover/twitter.png" alt="" title="">
                                <img src="images/sociable/team-social/twitter.png" alt="" title="">
                            </a>
                        </li>
                        <li>
                            <a href="" title="">
                                <img src="images/sociable/hover/facebook.png" alt="" title="">
                                <img src="images/sociable/team-social/facebook.png" alt="" title="">
                            </a>
                        </li>
                        -->
                        <li>
                            <a href="https://www.xing.com/profile/Aneta_Hadyk" title="">
                                <img src="images/sociable/hover/xing.png" alt="" title="">
                                <img src="images/sociable/hover/xing.png" alt="" title="">
                            </a>
                        </li>
                        <!--
                        <li>
                            <a href="" title="">
                                <img src="images/sociable/hover/google.png" alt="" title="">
                                <img src="images/sociable/team-social/google.png" alt="" title="">
                            </a>
                        </li>
                        -->
                    </ul>
                </div><!-- **Team - End** -->   
            </div>
            
            <div class="column one-third last">
            	<!-- **Team** -->   
            	<div class="team">
                	<div class="image">
	                	<img src="./images/team/Foto%205.JPG" alt="" title="">
                    </div>
                    <h4> Stefan Birkholz </h4>
                    <h6> Dipl.Phys. / Softwareentwickler </h6>
                    <ul class="social-icons">
                        <!--
                        <li>
                            <a href="" title="">
                                <img src="images/sociable/hover/twitter.png" alt="" title="">
                                <img src="images/sociable/team-social/twitter.png" alt="" title="">
                            </a>
                        </li>
                        <li>
                            <a href="" title="">
                                <img src="images/sociable/hover/facebook.png" alt="" title="">
                                <img src="images/sociable/team-social/facebook.png" alt="" title="">
                            </a>
                        </li>
                        -->
                        <li>
                            <a href="" title="">
                                <img src="images/sociable/hover/xing.png" alt="" title="">
                                <img src="images/sociable/hover/xing.png" alt="" title="">
                            </a>
                        </li>
                        <!--
                        <li>
                            <a href="" title="">
                                <img src="images/sociable/hover/google.png" alt="" title="">
                                <img src="images/sociable/team-social/google.png" alt="" title="">
                            </a>
                        </li>
                        -->
                    </ul>
                </div><!-- **Team - End** -->   
            </div>

        </section><!-- **Primary Section** -->      
        
        </div><!-- **Container - End** -->
    </div><!-- **Main - End** -->

<?php echo $txt_footer ?>

</div><!-- **Wrapper - End** -->


<!-- **jQuery** -->
<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery.mobilemenu.js"></script>


<script src="js/twitter/jquery.tweet.min.js"></script>

<script src="js/custom.js"></script>

</body>
</html>
