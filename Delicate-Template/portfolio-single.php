<?php
$current_file = basename(__FILE__);
include "php/languages/german.php" ;
echo $txt_header;
?>

    <!-- **Main** -->
    <div id="main">
    
    	<!-- **Breadcrumb** -->
    	<section class="breadcrumb-section">
        	<div class="container">
            	<div class="breadcrumb">
                    <a href="index.html"> Home </a> 
                    <span class="icon-chevron-right"> </span>
                    <h1> Portfolio Single </h1>
                </div>
                <div class="main-phone-no">
                	<p> 1 (800) 567 8765 <br> <a href="" title=""> name@somemail.com </a> </p>
                </div>
            </div>
        </section><!-- **Breadcrumb** -->
    
        <!-- **Container** -->
        <div class="container">
        
        <!-- **Primary Section** -->
        <section id="primary" class="content-full-width">     
        
        	<!-- **Portfolio Detail** -->
        	<div class="portfolio-single">
            
                <div class="column two-third">
                	<div class="portfolio-slider-container">
                        <ul class="portfolio-slider">
                            <li> <img src="http://placehold.it/700x475.jpg" alt="" title=""> </li>
                            <li> <img src="http://placehold.it/700x475.jpg" alt="" title=""> </li>
                            <li> <img src="http://placehold.it/700x475.jpg" alt="" title=""> </li>
                            <li> <img src="http://placehold.it/700x475.jpg" alt="" title=""> </li>
                        </ul>
                    </div>
                </div>
                
                <div class="column one-third last">
                	<h3> Lorem ipsum dolor sit amet, consect </h3>
                    <h6> Pellentesque habitant </h6>
                    <p>  Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec sit amet facilisis urna. Cras placerat ipsum nulla. Vestibulum quam diam, auctor vitae pretium quis, interdum quis nulla. Curabitur vulputate massa lorem. Proin nec ante porttitor sapien dapibus volutpat. Quisque quis ante dui, sed iaculis dui. Morbi vitae rhoncus erat. Duis nec leo massa. </p>
                    
                    <h5> Project Details </h5>                    
                    <p> <span class="icon-user"> </span> <strong> Client Name : </strong> designthemes </p>
                    <p> <span class="icon-map-marker"> </span> <strong> Location : </strong> Melbourne, Australia </p>
                    <p> <span class="icon-link"> </span> <strong> Website : </strong> <a href="" title=""> http://designthemes.com </a> </p>
                    <div class="portfolio-share"> <img src="images/post-images/portfolio-share.jpg" alt="" title=""> </div>
                </div>
                
                <div class="post-nav-container">
                    <div class="post-prev-link"><a rel="prev" href=""> <i class="icon-circle-arrow-left"> </i> Vestibulum congue <span>(Prev Entry)</span></a> </div>
                    <div class="post-next-link"><a rel="next" href=""> <span>(Next Entry)</span> Maecenas ut sem neque  <i class="icon-circle-arrow-right"> </i> </a></div>
                </div>    
                            
            </div><!-- **Portfolio Detail End** -->
            
            <div class="hr-invisible-small"> </div>
            
            <div class="border-title"> <h2> Related Projects <span> </span> </h2> </div>
            
            <!-- **Portfolio Carousel Wrapper** -->
            <div class="portfolio-carousel-wrapper gallery">
            	<!-- **Portfolio Carousel** -->
                <ul class="portfolio-carousel">
                    <li class="portfolio three-column">
                        <div class="portfolio-thumb">
                            <img src="http://placehold.it/520x350.jpg" alt="" title="">
                            <div class="image-overlay">
                                <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.html" title=""> Nullam dignisim vivera </a> </h5>
                            <p> Pellentesque habitant </p>
                        </div>
                    </li>
                    <li class="portfolio three-column">
                        <div class="portfolio-thumb">
                            <img src="http://placehold.it/520x350.jpg" alt="" title="">
                            <div class="image-overlay">
                                <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.html" title=""> Nullam dignisim vivera </a> </h5>
                            <p> Pellentesque habitant </p>
                        </div>
                    </li>
                    <li class="portfolio three-column last">
                        <div class="portfolio-thumb">
                            <img src="http://placehold.it/520x350.jpg" alt="" title="">
                            <div class="image-overlay">
                                <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.html" title=""> Nullam dignisim vivera </a> </h5>
                            <p> Pellentesque habitant </p>
                        </div>
                    </li>
                    <li class="portfolio three-column">
                        <div class="portfolio-thumb">
                            <img src="http://placehold.it/520x350.jpg" alt="" title="">
                            <div class="image-overlay">
                                <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.html" title=""> Nullam dignisim vivera </a> </h5>
                            <p> Pellentesque habitant </p>
                        </div>
                    </li>
                    <li class="portfolio three-column">
                        <div class="portfolio-thumb">
                            <img src="http://placehold.it/520x350.jpg" alt="" title="">
                            <div class="image-overlay">
                                <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.html" title=""> Nullam dignisim vivera </a> </h5>
                            <p> Pellentesque habitant </p>
                        </div>
                    </li>
                    <li class="portfolio three-column last">
                        <div class="portfolio-thumb">
                            <img src="http://placehold.it/520x350.jpg" alt="" title="">
                            <div class="image-overlay">
                                <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                                <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                            </div>
                        </div>
                        <div class="portfolio-detail">
                            <h5> <a href="portfolio-single.html" title=""> Nullam dignisim vivera </a> </h5>
                            <p> Pellentesque habitant </p>
                        </div>
                    </li>
           		</ul><!-- **Portfolio Carousel - End** -->
                    
                <div class="carousel-arrows">
                    <a href="#" title="" class="portfolio-prev-arrow"> <span class="icon-chevron-left"> </span> </a>
                    <a href="#" title="" class="portfolio-next-arrow"> <span class="icon-chevron-right"> </span> </a>
                </div>
                    
            </div><!-- **Portfolio Carousel Wrapper - End** -->
            
        
        </section><!-- **Primary Section** -->      
        
        </div><!-- **Container - End** -->
    </div><!-- **Main - End** -->

<?php echo $txt_footer ?>
	
</div><!-- **Wrapper - End** -->


<!-- **jQuery** -->
<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery.mobilemenu.js"></script>

<script src="js/jquery.viewport.js"></script>

<!-- Pretty Photo -->
<script src="js/jquery.prettyPhoto.js"></script>

<script src="js/jquery.carouFredSel-6.2.0-packed.js"></script>

<script src="js/jquery.bxslider.min.js"></script>

<script src="js/twitter/jquery.tweet.min.js"></script>

<script src="js/custom.js"></script>

</body>
</html>
