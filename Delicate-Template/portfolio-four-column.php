<?php
$current_file = basename(__FILE__);
include "php/languages/german.php" ;
echo $txt_header;
?>

    <!-- **Main** -->
    <div id="main">

        <?php echo $breadcrumb; ?>
    
        <!-- **Container** -->
        <div class="container">
        
        <!-- **Primary Section** -->
        <section id="primary" class="content-full-width">     
        
        	<div class="sorting-container">
            	<a href="#" title="" class="active-sort" data-filter=".all-sort"> All </a>
                <a href="#" title="" data-filter=".webdesign-sort"> webdesign </a>
                <a href="#" title="" data-filter=".html-sort"> html </a>
                <a href="#" title="" data-filter=".woocommerce-sort"> woo commerce </a>
                <a href="#" title="" data-filter=".blog-sort"> blog </a>
                <a href="#" title="" data-filter=".news-sort"> news </a>
                <a href="#" title="" data-filter=".photography-sort"> photography </a>
            </div>
            
        	<div class="portfolio-container gallery">        
            	<div class="portfolio four-column all-sort woocommerce-sort">
                	<div class="portfolio-thumb">
                    	<img src="http://placehold.it/520x350.jpg" alt="" title="">
                        <div class="image-overlay">
                            <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                            <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
                    </div>
                    <div class="portfolio-detail">
                        <h5> <a href="portfolio-single.html" title=""> Nullam dignisim vivera </a> </h5>
                        <p> Pellentesque habitant </p>
                    </div>
                </div>
            	<div class="portfolio four-column all-sort webdesign-sort">
                	<div class="portfolio-thumb">
                    	<img src="http://placehold.it/520x350.jpg" alt="" title="">
                        <div class="image-overlay">
                            <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                            <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
                    </div>
                    <div class="portfolio-detail">
                        <h5> <a href="portfolio-single.html" title=""> Nullam dignisim vivera </a> </h5>
                        <p> Pellentesque habitant </p>
                    </div>
                </div>
            	<div class="portfolio four-column all-sort webdesign-sort blog-sort">
                	<div class="portfolio-thumb">
                    	<img src="http://placehold.it/520x350.jpg" alt="" title="">
                        <div class="image-overlay">
                            <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                            <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
                    </div>
                    <div class="portfolio-detail">
                        <h5> <a href="portfolio-single.html" title=""> Nullam dignisim vivera </a> </h5>
                        <p> Pellentesque habitant </p>
                    </div>
                </div>  
            	<div class="portfolio four-column all-sort html-sort blog-sort woocommerce-sort">
                	<div class="portfolio-thumb">
                    	<img src="http://placehold.it/520x350.jpg" alt="" title="">
                        <div class="image-overlay">
                            <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                            <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
                    </div>
                    <div class="portfolio-detail">
                        <h5> <a href="portfolio-single.html" title=""> Nullam dignisim vivera </a> </h5>
                        <p> Pellentesque habitant </p>
                    </div>
                </div>
            	<div class="portfolio four-column all-sor webdesign-sort html-sort">
                	<div class="portfolio-thumb">
                    	<img src="http://placehold.it/520x350.jpg" alt="" title="">
                        <div class="image-overlay">
                            <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                            <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
                    </div>
                    <div class="portfolio-detail">
                        <h5> <a href="portfolio-single.html" title=""> Nullam dignisim vivera </a> </h5>
                        <p> Pellentesque habitant </p>
                    </div>
                </div>
            	<div class="portfolio four-column all-sort html-sort news-sort">
                	<div class="portfolio-thumb">
                    	<img src="http://placehold.it/520x350.jpg" alt="" title="">
                        <div class="image-overlay">
                            <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                            <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
                    </div>
                    <div class="portfolio-detail">
                        <h5> <a href="portfolio-single.html" title=""> Nullam dignisim vivera </a> </h5>
                        <p> Pellentesque habitant </p>
                    </div>
                </div> 
            	<div class="portfolio four-column all-sort html-sort blog-sort woocommerce-sort">
                	<div class="portfolio-thumb">
                    	<img src="http://placehold.it/520x350.jpg" alt="" title="">
                        <div class="image-overlay">
                            <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                            <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
                    </div>
                    <div class="portfolio-detail">
                        <h5> <a href="portfolio-single.html" title=""> Nullam dignisim vivera </a> </h5>
                        <p> Pellentesque habitant </p>
                    </div>
                </div>
            	<div class="portfolio four-column all-sort blog-sort news-sort">
                	<div class="portfolio-thumb">
                    	<img src="http://placehold.it/520x350.jpg" alt="" title="">
                        <div class="image-overlay">
                            <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                            <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
                    </div>
                    <div class="portfolio-detail">
                        <h5> <a href="portfolio-single.html" title=""> Nullam dignisim vivera </a> </h5>
                        <p> Pellentesque habitant </p>
                    </div>
                </div> 
            	<div class="portfolio four-column all-sort news-sort woocommerce-sort">
                	<div class="portfolio-thumb">
                    	<img src="http://placehold.it/520x350.jpg" alt="" title="">
                        <div class="image-overlay">
                            <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                            <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
                    </div>
                    <div class="portfolio-detail">
                        <h5> <a href="portfolio-single.html" title=""> Nullam dignisim vivera </a> </h5>
                        <p> Pellentesque habitant </p>
                    </div>
                </div>
            	<div class="portfolio four-column all-sort news-sort woocommerce-sort photography-sort">
                	<div class="portfolio-thumb">
                    	<img src="http://placehold.it/520x350.jpg" alt="" title="">
                        <div class="image-overlay">
                            <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                            <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
                    </div>
                    <div class="portfolio-detail">
                        <h5> <a href="portfolio-single.html" title=""> Nullam dignisim vivera </a> </h5>
                        <p> Pellentesque habitant </p>
                    </div>
                </div> 
            	<div class="portfolio four-column all-sort news-sort">
                	<div class="portfolio-thumb">
                    	<img src="http://placehold.it/520x350.jpg" alt="" title="">
                        <div class="image-overlay">
                            <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                            <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
                    </div>
                    <div class="portfolio-detail">
                        <h5> <a href="portfolio-single.html" title=""> Nullam dignisim vivera </a> </h5>
                        <p> Pellentesque habitant </p>
                    </div>
                </div>
            	<div class="portfolio four-column all-sort photography-sort">
                	<div class="portfolio-thumb">
                    	<img src="http://placehold.it/520x350.jpg" alt="" title="">
                        <div class="image-overlay">
                            <a href="http://placehold.it/520x350.jpg" data-gal="prettyPhoto[gallery]" title="" class="zoom"> <span class="icon-fullscreen"> </span> </a>
                            <a href="http://iamdesigning.com" target="_blank" title="" class="link"> <span class="icon-external-link"> </span> </a>
                        </div>
                    </div>
                    <div class="portfolio-detail">
                        <h5> <a href="portfolio-single.html" title=""> Nullam dignisim vivera </a> </h5>
                        <p> Pellentesque habitant </p>
                    </div>
                </div> 
        	</div>     
            
            <div class="clear"> </div>
            <div class="hr-invisible-small"> </div>   
            
            <div class="pagination"> 
            	<ul>
                    <li> <a href="" title=""> <span class="icon-double-angle-left"></span>Prev </a> </li>
                	<li> <a href="" title=""> 1 </a> </li>
                    <li class="active-page"> 2 </li>
                    <li> <a href="" title=""> 3 </a> </li>
                    <li> <a href="" title=""> 4 </a> </li>
                    <li> <a href="" title=""> Next<span class="icon-double-angle-right"></span>  </a> </li>
                </ul>
            </div>
        
        </section><!-- **Primary Section** -->      
        
        </div><!-- **Container - End** -->
    </div><!-- **Main - End** -->

<?php echo $txt_footer ?>
	
</div><!-- **Wrapper - End** -->


<!-- **jQuery** -->
<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery.mobilemenu.js"></script>

<script src="js/jquery.viewport.js"></script>

<!-- Pretty Photo -->
<script src="js/jquery.prettyPhoto.js"></script>

<script src="js/isotope.js"></script>
<script src="js/jquery.smartresize.js"></script>

<script src="js/twitter/jquery.tweet.min.js"></script>

<script src="js/custom.js"></script>

</body>
</html>
