<?php
$current_file = basename(__FILE__);
include "php/languages/german.php" ;
echo $txt_header;
?>
    
    <!-- **Main** -->
    <div id="main">

        <?php echo $breadcrumb; ?>
    
        <!-- **Container** -->
        <div class="container">
        
            <!-- **Primary Section** -->
            <section id="primary" class="content-full-width">     
            
                <div class="column one-column blog-thumb">
                    <!-- **Blog Entry** -->
                    <article class="blog-entry">                
                        <div class="entry-thumb-meta">
                            <div class="entry-thumb">
                                <a href="blog-single.html" title=""> <img src="http://placehold.it/340x230.jpg" alt="" title="" /> </a>
                            </div>
                            <div class="entry-meta"> 
                                <div class="date">
                                    <span class="icon-calendar"> </span>
                                    <p> April 06 2013 </p>
                                </div>
                                <a href="blog-single.html" title="" class="comments"> <span class="icon-comments"> </span> 16 </a>
                                <span class="rounded-bend"> </span>
                            </div>
                        </div>
                        
                        <div class="entry-details">                        
                            <div class="entry-title">
                                <h4> <a href="blog-single.html" title=""> Nullam dignisim vivera sceler </a> </h4>
                            </div>
                            
                            <div class="entry-metadata">
                                <div class="tags"> <span class="icon-tags"> </span> <a href="" title=""> teger</a>, <a href="" title=""> luctus</a>, <a href="" title=""> mor</a>, <a href="" title=""> purus </a> </div>
                            </div>
                            
                            <div class="entry-body">
                                <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eget lacus quam. Nam accumsan, sem vitae lobortis adipiscing, nulla leo ornare nisi, non ornare sapien nisi eu erat. Mauris quis neque at libero dignissim tincidunt. Vestibulum consequat, arcu sit amet gravida commodo.  </p>
                                <a href="blog-single.html" title="" class="read-more"> Read More <span class="icon-double-angle-right"> </span> </a>
                            </div>
                        </div>                    
                    </article><!-- **Blog Entry - End** -->
                    <div class="hr"> </div>
                </div>            
                
                <div class="column one-column blog-thumb">
                    <!-- **Blog Entry** -->
                    <article class="blog-entry">                
                        <div class="entry-thumb-meta">
                            <div class="entry-thumb">
                                <a href="blog-single.html" title=""> <img src="http://placehold.it/340x230.jpg" alt="" title="" /> </a>
                            </div>
                            <div class="entry-meta"> 
                                <div class="date">
                                    <span class="icon-calendar"> </span>
                                    <p> April 06 2013 </p>
                                </div>
                                <a href="blog-single.html" title="" class="comments"> <span class="icon-comments"> </span> 16 </a>
                                <span class="rounded-bend"> </span>
                            </div>
                        </div>
                        
                        <div class="entry-details">                        
                            <div class="entry-title">
                                <h4> <a href="blog-single.html" title=""> Nullam dignisim vivera sceler </a> </h4>
                            </div>
                            
                            <div class="entry-metadata">
                                <div class="tags"> <span class="icon-tags"> </span> <a href="" title=""> teger</a>, <a href="" title=""> luctus</a>, <a href="" title=""> mor</a>, <a href="" title=""> purus </a> </div>
                            </div>
                            
                            <div class="entry-body">
                                <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eget lacus quam. Nam accumsan, sem vitae lobortis adipiscing, nulla leo ornare nisi, non ornare sapien nisi eu erat. Mauris quis neque at libero dignissim tincidunt. Vestibulum consequat, arcu sit amet gravida commodo.  </p>
                                <a href="blog-single.html" title="" class="read-more"> Read More <span class="icon-double-angle-right"> </span> </a>
                            </div>
                        </div>                    
                    </article><!-- **Blog Entry - End** -->
                    <div class="hr"> </div>
                </div>
                
                <div class="column one-column blog-thumb">
                    <!-- **Blog Entry** -->
                    <article class="blog-entry">                
                        <div class="entry-thumb-meta">
                            <div class="entry-thumb">
                                <a href="blog-single.html" title=""> <img src="http://placehold.it/340x230.jpg" alt="" title="" /> </a>
                            </div>
                            <div class="entry-meta"> 
                                <div class="date">
                                    <span class="icon-calendar"> </span>
                                    <p> April 06 2013 </p>
                                </div>
                                <a href="blog-single.html" title="" class="comments"> <span class="icon-comments"> </span> 16 </a>
                                <span class="rounded-bend"> </span>
                            </div>
                        </div>
                        
                        <div class="entry-details">                        
                            <div class="entry-title">
                                <h4> <a href="blog-single.html" title=""> Nullam dignisim vivera sceler </a> </h4>
                            </div>
                            
                            <div class="entry-metadata">
                                <div class="tags"> <span class="icon-tags"> </span> <a href="" title=""> teger</a>, <a href="" title=""> luctus</a>, <a href="" title=""> mor</a>, <a href="" title=""> purus </a> </div>
                            </div>
                            
                            <div class="entry-body">
                                <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eget lacus quam. Nam accumsan, sem vitae lobortis adipiscing, nulla leo ornare nisi, non ornare sapien nisi eu erat. Mauris quis neque at libero dignissim tincidunt. Vestibulum consequat, arcu sit amet gravida commodo.  </p>
                                <a href="blog-single.html" title="" class="read-more"> Read More <span class="icon-double-angle-right"> </span> </a>
                            </div>
                        </div>                    
                    </article><!-- **Blog Entry - End** -->
                    <div class="hr"> </div>
                </div>
                
                <div class="column one-column blog-thumb">
                    <!-- **Blog Entry** -->
                    <article class="blog-entry">                
                        <div class="entry-thumb-meta">
                            <div class="entry-thumb">
                                <a href="blog-single.html" title=""> <img src="http://placehold.it/340x230.jpg" alt="" title="" /> </a>
                            </div>
                            <div class="entry-meta"> 
                                <div class="date">
                                    <span class="icon-calendar"> </span>
                                    <p> April 06 2013 </p>
                                </div>
                                <a href="blog-single.html" title="" class="comments"> <span class="icon-comments"> </span> 16 </a>
                                <span class="rounded-bend"> </span>
                            </div>
                        </div>
                        
                        <div class="entry-details">                        
                            <div class="entry-title">
                                <h4> <a href="blog-single.html" title=""> Nullam dignisim vivera sceler </a> </h4>
                            </div>
                            
                            <div class="entry-metadata">
                                <div class="tags"> <span class="icon-tags"> </span> <a href="" title=""> teger</a>, <a href="" title=""> luctus</a>, <a href="" title=""> mor</a>, <a href="" title=""> purus </a> </div>
                            </div>
                            
                            <div class="entry-body">
                                <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eget lacus quam. Nam accumsan, sem vitae lobortis adipiscing, nulla leo ornare nisi, non ornare sapien nisi eu erat. Mauris quis neque at libero dignissim tincidunt. Vestibulum consequat, arcu sit amet gravida commodo.  </p>
                                <a href="blog-single.html" title="" class="read-more"> Read More <span class="icon-double-angle-right"> </span> </a>
                            </div>
                        </div>                    
                    </article><!-- **Blog Entry - End** -->
                    <div class="hr"> </div>
                </div>
                
                <div class="column one-column blog-thumb">
                    <!-- **Blog Entry** -->
                    <article class="blog-entry">                
                        <div class="entry-thumb-meta">
                            <div class="entry-thumb">
                                <a href="blog-single.html" title=""> <img src="http://placehold.it/340x230.jpg" alt="" title="" /> </a>
                            </div>
                            <div class="entry-meta"> 
                                <div class="date">
                                    <span class="icon-calendar"> </span>
                                    <p> April 06 2013 </p>
                                </div>
                                <a href="blog-single.html" title="" class="comments"> <span class="icon-comments"> </span> 16 </a>
                                <span class="rounded-bend"> </span>
                            </div>
                        </div>
                        
                        <div class="entry-details">                        
                            <div class="entry-title">
                                <h4> <a href="blog-single.html" title=""> Nullam dignisim vivera sceler </a> </h4>
                            </div>
                            
                            <div class="entry-metadata">
                                <div class="tags"> <span class="icon-tags"> </span> <a href="" title=""> teger</a>, <a href="" title=""> luctus</a>, <a href="" title=""> mor</a>, <a href="" title=""> purus </a> </div>
                            </div>
                            
                            <div class="entry-body">
                                <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eget lacus quam. Nam accumsan, sem vitae lobortis adipiscing, nulla leo ornare nisi, non ornare sapien nisi eu erat. Mauris quis neque at libero dignissim tincidunt. Vestibulum consequat, arcu sit amet gravida commodo.  </p>
                                <a href="blog-single.html" title="" class="read-more"> Read More <span class="icon-double-angle-right"> </span> </a>
                            </div>
                        </div>                    
                    </article><!-- **Blog Entry - End** -->
                    <div class="hr"> </div>
                </div>
                
                <div class="column one-column blog-thumb">
                    <!-- **Blog Entry** -->
                    <article class="blog-entry">                
                        <div class="entry-thumb-meta">
                            <div class="entry-thumb">
                                <a href="blog-single.html" title=""> <img src="http://placehold.it/340x230.jpg" alt="" title="" /> </a>
                            </div>
                            <div class="entry-meta"> 
                                <div class="date">
                                    <span class="icon-calendar"> </span>
                                    <p> April 06 2013 </p>
                                </div>
                                <a href="blog-single.html" title="" class="comments"> <span class="icon-comments"> </span> 16 </a>
                                <span class="rounded-bend"> </span>
                            </div>
                        </div>
                        
                        <div class="entry-details">                        
                            <div class="entry-title">
                                <h4> <a href="blog-single.html" title=""> Nullam dignisim vivera sceler </a> </h4>
                            </div>
                            
                            <div class="entry-metadata">
                                <div class="tags"> <span class="icon-tags"> </span> <a href="" title=""> teger</a>, <a href="" title=""> luctus</a>, <a href="" title=""> mor</a>, <a href="" title=""> purus </a> </div>
                            </div>
                            
                            <div class="entry-body">
                                <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eget lacus quam. Nam accumsan, sem vitae lobortis adipiscing, nulla leo ornare nisi, non ornare sapien nisi eu erat. Mauris quis neque at libero dignissim tincidunt. Vestibulum consequat, arcu sit amet gravida commodo.  </p>
                                <a href="blog-single.html" title="" class="read-more"> Read More <span class="icon-double-angle-right"> </span> </a>
                            </div>
                        </div>                    
                    </article><!-- **Blog Entry - End** -->
                    <div class="hr"> </div>
                </div>
                
                <div class="column one-column blog-thumb">
                    <!-- **Blog Entry** -->
                    <article class="blog-entry">                
                        <div class="entry-thumb-meta">
                            <div class="entry-thumb">
                                <a href="blog-single.html" title=""> <img src="http://placehold.it/340x230.jpg" alt="" title="" /> </a>
                            </div>
                            <div class="entry-meta"> 
                                <div class="date">
                                    <span class="icon-calendar"> </span>
                                    <p> April 06 2013 </p>
                                </div>
                                <a href="blog-single.html" title="" class="comments"> <span class="icon-comments"> </span> 16 </a>
                                <span class="rounded-bend"> </span>
                            </div>
                        </div>
                        
                        <div class="entry-details">                        
                            <div class="entry-title">
                                <h4> <a href="blog-single.html" title=""> Nullam dignisim vivera sceler </a> </h4>
                            </div>
                            
                            <div class="entry-metadata">
                                <div class="tags"> <span class="icon-tags"> </span> <a href="" title=""> teger</a>, <a href="" title=""> luctus</a>, <a href="" title=""> mor</a>, <a href="" title=""> purus </a> </div>
                            </div>
                            
                            <div class="entry-body">
                                <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eget lacus quam. Nam accumsan, sem vitae lobortis adipiscing, nulla leo ornare nisi, non ornare sapien nisi eu erat. Mauris quis neque at libero dignissim tincidunt. Vestibulum consequat, arcu sit amet gravida commodo.  </p>
                                <a href="blog-single.html" title="" class="read-more"> Read More <span class="icon-double-angle-right"> </span> </a>
                            </div>
                        </div>                    
                    </article><!-- **Blog Entry - End** -->
                    <div class="hr"> </div>
                </div>
                
                <div class="column one-column blog-thumb">
                    <!-- **Blog Entry** -->
                    <article class="blog-entry">                
                        <div class="entry-thumb-meta">
                            <div class="entry-thumb">
                                <a href="blog-single.html" title=""> <img src="http://placehold.it/340x230.jpg" alt="" title="" /> </a>
                            </div>
                            <div class="entry-meta"> 
                                <div class="date">
                                    <span class="icon-calendar"> </span>
                                    <p> April 06 2013 </p>
                                </div>
                                <a href="blog-single.html" title="" class="comments"> <span class="icon-comments"> </span> 16 </a>
                                <span class="rounded-bend"> </span>
                            </div>
                        </div>
                        
                        <div class="entry-details">                        
                            <div class="entry-title">
                                <h4> <a href="blog-single.html" title=""> Nullam dignisim vivera sceler </a> </h4>
                            </div>
                            
                            <div class="entry-metadata">
                                <div class="tags"> <span class="icon-tags"> </span> <a href="" title=""> teger</a>, <a href="" title=""> luctus</a>, <a href="" title=""> mor</a>, <a href="" title=""> purus </a> </div>
                            </div>
                            
                            <div class="entry-body">
                                <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eget lacus quam. Nam accumsan, sem vitae lobortis adipiscing, nulla leo ornare nisi, non ornare sapien nisi eu erat. Mauris quis neque at libero dignissim tincidunt. Vestibulum consequat, arcu sit amet gravida commodo.  </p>
                                <a href="blog-single.html" title="" class="read-more"> Read More <span class="icon-double-angle-right"> </span> </a>
                            </div>
                        </div>                    
                    </article><!-- **Blog Entry - End** -->
                    <div class="hr"> </div>
                </div>
                
                <div class="pagination"> 
                    <ul>
                        <li> <a href="" title=""> <span class="icon-double-angle-left"></span>Prev </a> </li>
                        <li> <a href="" title=""> 1 </a> </li>
                        <li class="active-page"> 2 </li>
                        <li> <a href="" title=""> 3 </a> </li>
                        <li> <a href="" title=""> 4 </a> </li>
                        <li> <a href="" title=""> Next<span class="icon-double-angle-right"></span>  </a> </li>
                    </ul>
                </div>
            
            </section><!-- **Primary Section** -->      
            
        </div><!-- **Container - End** -->
    </div><!-- **Main - End** -->

<?php echo $txt_footer ?>
	
</div><!-- **Wrapper - End** -->


<!-- **jQuery** -->
<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery.mobilemenu.js"></script>

<script src="js/jquery.viewport.js"></script>

<script src="js/twitter/jquery.tweet.min.js"></script>

<script src="js/custom.js"></script>

</body>
</html>
