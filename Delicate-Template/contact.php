<?php
$current_file = basename(__FILE__);
include "php/languages/german.php" ;
echo $txt_header;
?>
    
    <!-- **Main** -->
    <div id="main">

        <?php echo $breadcrumb; ?>
    
        <!-- **Container** -->
        <div class="container">
        
        <!-- **Primary Section** -->
        <section id="primary" class="content-full-width">   
        
        	<div class="column two-third">
            	<div id="map"> </div>
            </div>  
            
            <div class="column one-third last">
            	<h3> Kontaktdetails </h3>
                <div class="contact-details">
                	<p> Ingenieurbüro Gregor Hadyk</p>
                    <p>Embedded Engineering </p>
                    <p> <span class="icon-phone"> </span> <strong>Phone</strong> : +49(0) 173 2175650 </p>
                    <p> <span class="icon-envelope-alt"> </span> <strong>Email</strong> : <a href="mailto:info@ibgh.biz"> info@ibgh.biz </a> </p>
                    <p> <span class="icon-globe"> </span> <strong>Website</strong> : <a href="http://www.ibgh.biz" title=""> www.ibgh.biz </a> </p>
                </div>
            </div>
            
            <div class="clear"> </div>
            <div class="hr-invisible"> </div> 
            <!--
            <h3> Send us a message </h3>
            <div class="message"></div>
            <form action="php/sendmail.php" method="get" id="contact-form">
                <p class="column one-half">
                    <input name="name" type="text" placeholder="Your Name" required>
                </p>
                
                <p class="column one-half last">
                    <input name="email" type="email" placeholder="Your Email" required>
                </p>
                
                <p class="clear">
                    <textarea name="comment" placeholder="Your Message" cols="5" rows="3" required></textarea>
                </p>
                
                <p>
                    <input name="submit" type="submit" value="Comment">
                </p>            
            </form>
        -->
        	
        </section><!-- **Primary Section** -->      
        
        </div><!-- **Container - End** -->
    </div><!-- **Main - End** -->

<?php echo $txt_footer ?>

</div><!-- **Wrapper - End** -->


<!-- **jQuery** -->
<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery.mobilemenu.js"></script>


<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="js/jquery.gmap.min.js"></script>

<script src="js/twitter/jquery.tweet.min.js"></script>

<script src="js/custom.js"></script>

</body>
</html>
