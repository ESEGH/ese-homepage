<?php
$current_file = basename(__FILE__);
include "php/languages/german.php" ;
echo $txt_header;
?>
    <!-- **Main** -->
    <div id="main">
    
    	<!-- **Breadcrumb** -->
    	<section class="breadcrumb-section">
        	<div class="container">
            	<div class="breadcrumb">
                    <a href="index.html"> Home </a> 
                    <span class="icon-chevron-right"> </span>
                    <h1> Error 404 Page </h1>
                </div>
                <div class="main-phone-no">
                	<p> 1 (800) 567 8765 <br> <a href="" title=""> name@somemail.com </a> </p>
                </div>
            </div>
        </section><!-- **Breadcrumb** -->
    
        <!-- **Container** -->
        <div class="container">
        
        <!-- **Primary Section** -->
        <section id="primary" class="content-full-width">     
        
            <div class="error-info">
                <h2> <span class="error"> 404! </span> <span> OOPS, </span> This Page Could Not Be Found! </h2>
                <h3> You may also want to search for what you are looking for. </h3>
                <form action="#" id="searchform" method="get">
                    <input type="text" placeholder="Search our site" name="s" id="s">
                    <input type="submit" name="submit" value="Go">
                </form>
            </div>            
        	
        </section><!-- **Primary Section** -->      
        
        </div><!-- **Container - End** -->
    </div><!-- **Main - End** -->
    
    <!-- **Footer** -->
    <footer id="footer">
    	<div class="container">
        
			<div class="column one-fourth"> 
                <aside class="widget">
                    <h3 class="widgettitle"> <a href="" title=""> Information </a> </h3>
                    <ul>
                        <li> <a href="" title=""> Lorem ipsum dolor sit amet </a> </li>
                      <li> <a href="" title=""> Mauris aliquam nisi eget quam mollis </a> </li>
                      <li> <a href="" title=""> Sed a ante nulla, id pretium </a> </li>
                      <li> <a href="" title=""> Nunc eleifend purus cursus risus </a> </li>
                      <li> <a href="" title=""> Vestibulum in eros lectus, at tempus </a> </li>        
                        <li> <a href="" title=""> Nunc eu tellus vitae lacus viverra </a> </li>
                    </ul>
                </aside>   
			</div>
            
            <div class="column one-fourth"> 
                <aside class="widget">
                    <h3 class="widgettitle"> <a href="" title=""> Featured Products </a> </h3>
                    <ul class="product_list_widget">
                    	<li> 
                        	<a href="" title=""> 
                            	<img src="http://placehold.it/70x60.jpg" alt="" title="">                                 
                            </a>
                            Integer luctus portto pur nec ornare metus
                            <span class="amount"> $170,000 </span>
                        </li>
                        <li> 
                        	<a href="" title=""> 
                            	<img src="http://placehold.it/70x60.jpg" alt="" title="">                                 
                            </a>
                            Integer luctus portto pur nec ornare metus
                            <span class="amount"> $170,000 </span>
                      </li>
                        <li> 
                        	<a href="" title=""> 
                            	<img src="http://placehold.it/70x60.jpg" alt="" title="">                                 
                            </a>
                            Integer luctus portto pur nec ornare metus
                            <span class="amount"> $170,000 </span>
                        </li>
                    </ul>
                </aside>   
			</div>
        
        
        	<div class="column one-fourth">
                <aside class="widget tweetbox"> 
                    <h3 class="widgettitle"> <a href="" title=""> Twitter Feeds </a> </h3>
                    <div class="tweets"> </div>
                </aside>   
            </div>  
            
            <div class="column one-fourth last">
            	<aside class="widget">
                    <h3 class="widgettitle"> <a href="" title=""> Contact Info </a> </h3>
                	<p> 105, B Block,  Wesr Street, London, BCA, United Kingdom </p>
                    <p> <span class="icon-phone"> </span> Phone : 1800-299-234-22 </p>
                    <p> <span class="icon-envelope-alt"> </span> Email : <a href="mailto:delicate@someemail.com"> delicate@someemail.com </a> </p>
                    <p> <span class="icon-globe"> </span> Website : <a href="" title=""> www.yourdomain.com </a> </p> 
                    <ul class="social-icons">
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/facebook.png" alt="" title="">
                                <img src="images/sociable/facebook.png" alt="" title="">                        
                            </a>
                        </li>
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/google.png" alt="" title="">
                                <img src="images/sociable/google.png" alt="" title="">                        
                            </a>
                        </li>
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/twitter.png" alt="" title="">
                                <img src="images/sociable/twitter.png" alt="" title="">                        
                            </a>
                        </li>
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/dribble.png" alt="" title="">
                                <img src="images/sociable/dribble.png" alt="" title="">                        
                            </a>
                        </li>
                        <li>
                            <a href="" title=""> 
                                <img src="images/sociable/hover/linkedin.png" alt="" title="">
                                <img src="images/sociable/linkedin.png" alt="" title="">                        
                            </a>
                        </li>
                    </ul>
                </aside>	
            </div>
            
        </div>
        
        <div class="copyright">        	
        	<div class="container">
                <p> Copyright &copy; 2013 Delicate Theme All Rights Reserved. | <a href="" title=""> Design Themes </a> </p>        	
            </div>
        </div>
    </footer><!-- **Footer - End** -->
	
</div><!-- **Wrapper - End** -->


<!-- **jQuery** -->
<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery.mobilemenu.js"></script>


<script src="js/twitter/jquery.tweet.min.js"></script>

<script src="js/custom.js"></script>

</body>
</html>
